package com.galvanize;

import java.util.*;

public class Algorithm {

    public boolean allEqual(String input){
        boolean theSame = false;
        input = input.toLowerCase();

        if (input.equals("") || input.length()==0){
            return theSame;
        }else{
            for (int i = 0; i < input.length(); i++) {
                if (input.charAt(0) != input.charAt(i)){
                    theSame = false;
                    return theSame;
                }else{
                    theSame = true;
                }
            }

        }

        return theSame;
    }

    public Map<String, Long> letterCount(String input) {
        HashMap<String, Long> result = new HashMap<>();
        long sumTotal = 0;
        input = input.toLowerCase();

//        for (int i = 0; i < input.length(); i++) {
//
//            for (int j = 0; j < input.length(); j++) {
//
//            }
//
//        }
        char[] inputArray = input.toCharArray();

        for (char c: inputArray) {
            if(result.containsKey(String.valueOf(c))){
                result.put(String.valueOf(c), result.get(String.valueOf(c)) + 1);
            }else{
                result.put(String.valueOf(c), 1L);
            }
        }



//        for (int i = input.length() -1; i>=0; i--) {
//            if (result.containsKey(input.charAt(i))) {
//                Long count = result.get(input.charAt(i));
//                result.put(String.valueOf(input.charAt(i)), count++);
//            } else {
//                result.put(String.valueOf(input.charAt(i)), (long) 1);
//            }


        //}
        System.out.println(result);
        return result;
    }

    public String interleave(List<String> list1, List<String> list2){
        String result = "";
        for (int i = 0; i < list1.size(); i++) {

            if(i < list1.size()){
                result+=list1.get(i)+list2.get(i);
            }


        }
        return result;
    }
}
