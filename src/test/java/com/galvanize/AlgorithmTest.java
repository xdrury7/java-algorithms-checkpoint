package com.galvanize;


import org.junit.jupiter.api.Test;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AlgorithmTest {

    @Test
    public void shouldBeAbleToUseAllEqualMethod (){
        //setup
        Algorithm algorithm = new Algorithm();
        algorithm.allEqual("aaa");
        //enact
        //assert
    }

    @Test
    public void shouldReturnFalseWithEmptyString(){
        //setup
        Algorithm test  = new Algorithm();

        //enact
        boolean result = test.allEqual("");
        //assert
        assertEquals(false, result);
    }

    @Test
    public void shouldReturnTrueIfAllLettersTheSameAndFalseIfNot(){
        //setup
        Algorithm test  = new Algorithm();

        //enact
        boolean result = test.allEqual("aaa");
        //assert
        assertEquals(true,result);
        //enact
        boolean result2 = test.allEqual("aaab");
        //assert
        assertEquals(false, result2);

    }

    @Test
    public void shouldRunAsCaseInsensitive(){
        //setup
        Algorithm test  = new Algorithm();

        //enact
        boolean result = test.allEqual("aAa");
        //assert
        assertEquals(true, result);

        boolean result2 = test.allEqual("bbBbabbb");
        assertEquals(false, result2);
    }

//    @Test
//    public void shouldInstantiateLetterCountMethod(){
//        Algorithm algorithm = new Algorithm();
//
//        algorithm.letterCount("aa");
//    }

    @Test
    public void shouldReturnNothingWithEmptyString(){
        Algorithm algorithm = new Algorithm();

        Map result = algorithm.letterCount("aa");

        assertTrue(result.containsKey("a"));

        Map result2 = algorithm.letterCount("aAbBcD");

        assertTrue(result2.containsKey("a"));

    }

    @Test
    public void foo(){
        Algorithm algorithm = new Algorithm();



        String result = algorithm.interleave(Arrays.asList("a","b", "c"), Arrays.asList("d", "e", "f"));

        assertEquals("adbecf", result);
    }

}
